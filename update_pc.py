import subprocess
import time

def run(*args):
    return subprocess.check_call(['git'] + list(args))

def pull_database():
    run("fetch")
    print(run("pull"))

print("Buscar actualizaciones durante...")
durante_mins = int(input("Minutos: "))
print("Buscar actualizaciones cada...")
periodo_segs = int(input("Segundos: "))

durante_segs = durante_mins * 60
ciclos_fetch = int(durante_segs / periodo_segs)

print(time.ctime())
pull_database()
for x in range(ciclos_fetch):
    time.sleep(periodo_segs)
    print(time.ctime())
    pull_database()