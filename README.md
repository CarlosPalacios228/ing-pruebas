# README #
* Simple repo to store our Ing. Pruebas database.
* The pi will automtically commit and push whenever it tests a new board.
* The pc will fetch and pull new results, periodically.

### How do I get set up? ###
1. Clone the repo in the desired direcotry.

#### pi
2. Set-up the fixture.
3. Place the reference board (if needed) and Run the new_ref.py 
   It will be stored in ./images
4. Then, place the new boards and run the cmp_pcb.py to test them.
5. Results are updated in the database.xlsx file and pushed to this repo.

#### pc
2. Run the update_pc.py and enter the total time (in minutes) that the session 
   will be active (fetching)

### Who do I talk to? ###
* Carlos Palacios
* Luis David Blanco
